// dependencies
const expect = require('chai').expect;
const puppeteer = require('puppeteer');



// pointer to our browser tab so it will persist between tests
let page;
const host = process.env.MOCHA_TEST_HOST;

// the test suite
describe('1nce website', function () {
  this.timeout(60000); // Useful when testing really slow sites

  // open a new browser tab and set the page variable to point at it
  before (async function () {
    global.expect = expect;
    global.browser = await puppeteer.launch( { headless: false  } ); //defaultViewport: {width: 1920, height: 1080}
    page = await browser.newPage();
    await page.setViewport({width: 1920, height: 1080});
  });

  // close the browser when the tests are finished
  after (async function () {
    await page.close();
    await browser.close();
  });

  //Tests go here
  it('homepage loads and has correct page title', async function () {
    const [response] = await Promise.all([
      page.goto('https://1nce.com/', {timeout:0}),
      //page.waitForTimeout(''),
    ]);
    expect(await page.title()).to.contain('IoT Flat Rate | Connectivity for M2M and IoT applications | 1NCE');
  });

  it('Check Login Portal button exists', async function () {


  });

  it('Check Buy button exists', async function () {



  });


});
