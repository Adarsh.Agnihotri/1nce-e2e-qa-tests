// dependencies
const expect = require('chai').expect;
const puppeteer = require('puppeteer');



// pointer to our browser tab so it will persist between tests
let page;
const host = process.env.MOCHA_TEST_HOST;

// the test suite
describe('My test suite', function () {
  this.timeout(60000); // Useful when testing really slow Drupal sites

  // open a new browser tab and set the page variable to point at it
  before (async function () {
    global.expect = expect;
    global.browser = await puppeteer.launch( { headless: false  } ); //defaultViewport: {width: 1920, height: 1080}
    page = await browser.newPage();
    await page.setViewport({width: 1920, height: 1080});
  });

  // close the browser when the tests are finished
  after (async function () {
    await page.close();
    await browser.close();
  });

  //Tests go here
  it('homepage loads and has correct page title', async function () {
    const [response] = await Promise.all([
      page.goto('https://portal.1nce.com/portal/customer/login', {timeout:0}),
      page.waitForNavigation({ waitUntil: 'networkidle0' }),
    ]);
    expect(await page.title()).to.contain('1NCE Portal | Manage your 1NCE IoT Flat Rate | 1NCE - IoT SIM');
  });

  
  it('login page and view dashboard', async function () {
    //page.goto('https://portal.1nce.com/portal/customer/login', {timeout:0} );
    page.waitForNavigation('#CybotCookiebotDialog', { waitUntil: "networkidle0" });
    const [cookie] = await Promise.all([
          page.click('#CybotCookiebotDialogBodyLevelButtonLevelOptinAllowAll'),
    ]);


    await page.type('#username', 'adarsh.agnihotri@1nce.com');
    await page.type('#password', 'Oncerule5');


    const [response] = await Promise.all([
      page.click('#login-submit-button'),
      console.log('User logged in to portal'),
    ]);

    await page.waitForNavigation('networkidle2');
    expect(await page.url()).to.contain('https://portal.1nce.com/portal/customer/dashboard');
    let DataVol = await page.$eval('.panel-header', e => e.innerText);
    expect(DataVol).to.include('Data Volume');
    let SIMStatus = await page.$eval('#A1F10E1AA730449E', e => e.innerText);
    expect(SIMStatus).to.include('Current SIM State');
    expect(await page.$eval('#headerCompanyName', e => e.innerText)).to.include('1nce (Adarsh)');
    //expect(SIMStatus).to.include('Activated 4');

  });

/*
it('Go to Performance Menu', async function (){
    const [performance] = await Promise.all([
        page.waitForNavigation('networkidle2'),
        page.click('#nav-customer-performance'),
      ]);
    expect(await page.url()).to.contain('https://portal.1nce.com/portal/customer/performance');
    let result = await page.$eval('#performance-table', e => e.innerText);
    expect(result).contains('2G/3G Network	Available');
    expect(result).contains('4G/LTE Network	Available');
    expect(result).contains('NB-IoT Network	Available');
    expect(result).contains('OpenVPN	Available');
    expect(result).contains('Management API	Available');
    expect(result).contains('Data Streams	Available');
})

it('Go to Customer SIMS for details', async function (){
    const [CustomerSims] = await Promise.all([
        page.waitForNavigation('networkidle2'),
        page.click('#nav-customer-sims'),
      ]);
      expect(await page.url()).to.contain('https://portal.1nce.com/portal/customer/sims');
      let SIMsDetails = await page.$eval('#table-row-2000003208', e => e.innerText)
      expect(SIMsDetails).contains('\n\t\t\n\t\n901405100002725\n\t\n8988280666000002725\n\t\n882285100002725\n\t\n3554900693976711\n\t\n10.232.130.1\n\t\n1NCE FlexSIM\n\t\t\n\t\t\t');

})  

it('Go to a single SIM and retrieve details', async function (){
    const [CustomerSims] = await Promise.all([
        page.waitForNavigation('networkidle2'),
        page.click('#nav-customer-sims'),
      ]);
      const [SIMDetail] = await Promise.all([
        page.waitForNavigation('networkidle2'),
        page.click('#table-row-2000003209'),
      ]);
      expect(await page.url()).to.contain('https://portal.1nce.com/portal/customer/sim/2000003209');
      let SIMNetworkDetails = await page.$eval('#simNetworkCollapse', e => e.innerText);
      expect(SIMNetworkDetails).contains('SIM Status\nActivated \nSession-Status\nAttached \nIMEI\n8684460384012507 \nIP-Address\n10.232.130.235\nOperator / Country\nT-Mobile / Germany\nAccess Technology\n');
      let SIMDetails = await page.$eval('#simDetailsCollapse', e => e.innerText);
      expect(SIMDetails).contains('ICCID\n8988280666000002726\nIMSI\n901405100002726\nMSISDN\n882285100002726\nLabel');

      let eventdetails = await page.$eval('#last-event-8988280666000002726', e => e.innerText);
      //expect(eventdetails).contains('Event	Timestamp	Source	IP');
      await page.click('.usage-tab');
      
    })


// it('Reset SIM Connectivity', async function (){   
//       const [SIMDetail] = await Promise.all([
//         page.waitForNavigation('networkidle2'),
//         page.click('#sim-action-reset-connectivity'),
//       ]);

//       await page.waitForTimeout(2000);
//       let resetConnectivity = await page.$eval('.notification-content', e => e.innerText);  
//       expect(resetConnectivity).contains('Connection reset initialized');

//     })      

it('Go to Customer Account Details', async function (){
    const [customerSims] = await Promise.all([
        page.click('#nav-customer-vpn'),
      ]);
        page.waitForNavigation({ waitUntil: 'domcontentloaded' });
        page.click('#networkPanel');
        page.select("#data-limit-select", "40 MB per 1 month"),
        page.click('#limits-submit-button')

    expect(await page.url()).to.contain('https://portal.1nce.com/portal/customer/configuration');
         //await page.waitForTimeout(15000);
        // const option = await page.$x('//*[@id="wrapper"]/div[2]/div/div', e => e.innerText );
        // console.log('Selected option is:', option)
        // expect(option).contains('The change to your monthly limits on SIMs was initiated successfully.');
})

it('User Details', async function (){
    const [customerSims] = await Promise.all([
        page.click('#nav-customer-users'),
        page.waitForNavigation({ waitUntil: 'domcontentloaded' })
      ]);
      expect(await page.url()).to.contain('https://portal.1nce.com/portal/customer/users?currentPage=1');

      //page.click('#table-row-4586819694');

    //let userEmailId = await page.$eval('#table-row-4586819694', e => e.innerText);
    //expect(userEmailId).contains('Adarsh');    
})

//const wait = () => new Promise((resolve) => setTimeout(resolve, 1000));
*/
it('Organisation Details', async function (){
    const [customerSims] = await Promise.all([
        page.click('#nav-customer-organisation'),
        page.waitForNavigation({ waitUntil: 'domcontentloaded' })
      ]);
      expect(await page.url()).to.contain('https://portal.1nce.com/portal/customer/organisation');

      const [newOrg] = await Promise.all([
        page.click('#startCreateSubunitButton'),
        page.waitForNavigation({ waitUntil: 'domcontentloaded' })
      ]);

      //await page.waitForNavigation({ waitUntil: 'load' });
      expect(await page.url()).to.contain('https://portal.1nce.com/portal/customer/organisation/suborganisation/register');
      let userEmailId = await page.$eval('#continueShopping', e => e.innerText);
      expect(userEmailId).contains('Next');
      let subOrgs = await page.$eval('#suborgs-table-wrapper', e => e.innerText);
      expect(subOrgs).contains('');

})   

it('Data Streamer', async function (){
  const [customerSims] = await Promise.all([
    page.click('#nav-customer-vpn'),
  ]);
  expect(await page.url()).to.contain('https://portal.1nce.com/portal/customer/configuration');
  const [customerSims1] = await Promise.all([
    page.click('#nav-customer-vpn'),
  ]);
  page.click('#dataStreamsPanel');
  page.waitForNavigation(2000);
  page.click('#new-data-stream-button-div');
  

})  


});